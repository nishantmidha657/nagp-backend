#Link for the code repository

    https://gitlab.com/nishantmidha657/nagp-backend.git

#Dockerhub url for docker images

    https://hub.docker.com/repository/docker/midhanishant3/nagp-backend/general

#URL for service API tier to view the records from backend tier

curl --location --request POST 'http://34.41.101.61:80/employee/new' \
--header 'Content-Type: application/json' \
--header 'Access-Control-Request-Headers: *' \
--header 'Accept: application/json, text/plain, */*' \
--data-raw '{
    "empId":"9",
    "empName":"John",
    "address":"USA"
}'


curl --location --request GET 'http://34.41.101.61:80/employee/details' \
--header 'Content-Type: application/json' \
--header 'Access-Control-Request-Headers: *' \
--header 'Accept: application/json, text/plain, */*'



#Link for the final recording

https://nagarro-my.sharepoint.com/:u:/p/nishant_midha/EQR1AJYIG7tDhcznFxtkjYUBypBnIm-SR_2FvrYqU_Q3Ig?e=ubk9mh

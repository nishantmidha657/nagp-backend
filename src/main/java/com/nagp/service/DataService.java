package com.nagp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.nagp.model.Employee;
import com.nagp.repository.EmployeeRepo;

@Component
public class DataService {

	@Autowired
	private EmployeeRepo repo;

	public List<Employee> getData() {
		return repo.findAll();
	}
	
	
	public void saveEmployee(Employee employee) {
		repo.save(employee);
	}
}

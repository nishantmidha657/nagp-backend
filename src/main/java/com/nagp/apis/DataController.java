package com.nagp.apis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nagp.model.Employee;
import com.nagp.service.DataService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("employee")
public class DataController {

	@Autowired
	private DataService dataService;

	@GetMapping("/details")
	public ResponseEntity<List<Employee>> employeeDetails() {
		return new ResponseEntity<>(dataService.getData(), HttpStatus.OK);
	}

	@PostMapping(value = "/new")
	public void saveDetails(@RequestBody Employee employee) {
		dataService.saveEmployee(employee);
	}

}
